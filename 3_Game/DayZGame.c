modded class DayZGame {
	
	protected int m_Cfg1 = 20; //ActionTime
	protected int m_Cfg2 = 1; //DropRuined
	
	Param2<int, int> DZRRBH_ReadServerConfig() {

			ref Param2<int, int> m_Data = new Param2<int, int>(m_Cfg1, m_Cfg2);
			return m_Data;

	}
	
	void DZRRBH_SaveConfigOnServer(int Cfg1, int Cfg2) {
		//Print("[dzr_rags_by_hand] ::: DayZGame ::: DZRRBH_SaveConfigOnServer");
		if(Cfg1 != 0)
		{
		m_Cfg1 = Cfg1;
		}
		//if(Cfg2 != 0)
		//{
		m_Cfg2 = Cfg2;
		//}
		Print("[dzr_rags_by_hand] ::: DayZGame ::: Saved Server Config values:" + m_Cfg1 +", "+  m_Cfg2);
	}
}